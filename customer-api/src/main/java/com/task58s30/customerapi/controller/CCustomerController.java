package com.task58s30.customerapi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.task58s30.customerapi.model.CCustomer;
import com.task58s30.customerapi.repository.ICustomerRepository;

@RestController
@CrossOrigin
public class CCustomerController {
    @Autowired
    ICustomerRepository customerRepository;

    @GetMapping("/customers")
    public ResponseEntity <List<CCustomer>> getAllCustomers() {
        try {
            List<CCustomer> customerList = new ArrayList<CCustomer>();
            customerRepository.findAll().forEach(customerList::add);
            return new ResponseEntity<>(customerList, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
