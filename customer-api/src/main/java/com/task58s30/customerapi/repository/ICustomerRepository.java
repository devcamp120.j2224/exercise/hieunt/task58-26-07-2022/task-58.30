package com.task58s30.customerapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.task58s30.customerapi.model.CCustomer;

public interface ICustomerRepository extends JpaRepository <CCustomer, Long> {
    
}
